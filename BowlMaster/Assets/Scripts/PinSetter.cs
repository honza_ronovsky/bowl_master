﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinSetter : MonoBehaviour {
    //design
    [SerializeField]
    GameObject pinsHolder;
    [SerializeField]
    Text uprightText;
    [SerializeField]
    float timeForSettle = 3f;
    [SerializeField]
    float distanceToRaise = 80f;
    [SerializeField]
    GameObject pinsHolderPrefab;
    [SerializeField]
    GameObject pinsHolderParent;

    //state
    int _pinsStanding;
    int lastStandingCount = 10;
    bool hasBallLeft = false;
    bool arePinsRaised = false;
    float lastChangeTime = 0;
    Vector3 pinsHolderInitPos;
    Quaternion pinsHolderInitRotation;

    public int PinsStanding {
        get {
            return _pinsStanding;
        }
        set {
            if (value != _pinsStanding) {
                lastChangeTime = Time.time;
            }
            _pinsStanding = value;
        }
    }

    //cache
    Pin[] pins;
    Ball ball;
    static ActionMaster actionMaster;
    Animator animator;

    void Awake() {
        animator = GetComponent<Animator>();
        ball = FindObjectOfType<Ball>();
        if (actionMaster == null) {
            actionMaster = new ActionMaster();
        }
    }

    void Start() {
        FindPins();
        pinsHolderInitPos = pinsHolder.transform.position;
        pinsHolderInitRotation = pinsHolder.transform.rotation;
    }

    public void FindPins() {
        pins = pinsHolder.GetComponentsInChildren<Pin>();
    }

    void Update() {
        if (hasBallLeft) {
            uprightText.color = Color.red;
            PinsStanding = CountStanding();
            CheckStanding();
        }
    }

    void CheckStanding() {
        if (Time.time - lastChangeTime > timeForSettle) {
            PinsHaveSettled();
        }
    }

    void PinsHaveSettled() {
        uprightText.text = PinsStanding.ToString();
        uprightText.color = Color.green;
        int knockedOverCount = lastStandingCount - PinsStanding;
        lastStandingCount = PinsStanding;
        ProcessKnockedPins(knockedOverCount);
        hasBallLeft = false;
        ball.Reset();
    }

    private void ProcessKnockedPins(int knockedOverCount) {
        ActionMaster.Action action = actionMaster.Bowl(knockedOverCount);
        SetAnimation(action);
    }

    private void SetAnimation(ActionMaster.Action action) {
        if (action == ActionMaster.Action.Tidy) {
            animator.SetTrigger("tidyTrigger");
        }
        if (action == ActionMaster.Action.Reset || action == ActionMaster.Action.EndTurn) {
            animator.SetTrigger("resetTrigger");
        }
    }

    int CountStanding() {
        int pinStandTemp = 0;
        foreach (Pin pin in pins) {
            if (pin)
                pinStandTemp += pin.IsStanding() ? 1 : 0;
        }
        return pinStandTemp;
    }

    public void SetBallLeft() {
        hasBallLeft = true;
        lastChangeTime = Time.time;
    }

    void OnTriggerExit(Collider coll) {
        Pin pin = coll.gameObject.GetComponentInParent<Pin>();
        if (pin)
            Destroy(pin.gameObject, 0.5f);
    }

    public void RaisePins() {
        if (!arePinsRaised) {
            foreach (Pin pin in pins) {
                if (pin) {
                    if (pin.IsStanding()) {
                        pin.setPinGravity(false);
                        pin.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                        pin.transform.Translate(0, distanceToRaise, 0, Space.World);
                    }
                }
            }
            arePinsRaised = true;
        }
    }

    public void LowerPins() {
        Debug.Log(arePinsRaised);
        if (arePinsRaised) {
            foreach (Pin pin in pins) { // We dont need check standing pins - not standing pins should be removed with swipe motion
                if (pin) {
                    pin.transform.Translate(0, -distanceToRaise, 0, Space.World);
                    pin.setPinGravity(true);
                }
            }
            arePinsRaised = false;
        }
    }

    public void RenewPins() {
        Vector3 adjPinsHolderInitPos = new Vector3(pinsHolderInitPos.x, pinsHolderInitPos.y, pinsHolderInitPos.z);
        Destroy(pinsHolder);
        pinsHolder = Instantiate(pinsHolderPrefab, adjPinsHolderInitPos, pinsHolderInitRotation, pinsHolderParent.transform);
        arePinsRaised = false;
        if (pinsHolder) {
            CameraControl cc = FindObjectOfType<CameraControl>();
            cc.SetStopObject(pinsHolder);
        }
        else {
            Debug.LogError("PinsHolder not instantiated");
        }
        FindPins();
        RaisePins();
        PinsStanding = 10; // not changing through property to avoid manipuolatin lastChangeTime
        lastStandingCount = 10;
        uprightText.text = PinsStanding.ToString();
        uprightText.color = Color.black;
    }

}
