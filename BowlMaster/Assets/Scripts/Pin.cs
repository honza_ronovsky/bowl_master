﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pin : MonoBehaviour {
    //design
    [SerializeField]
    float standingThresholdRotation;
    [SerializeField]
    float standingThresholdPositionY;

    //state
    bool hasStarted = false;

    //cache
    AudioSource audioSource;
    Rigidbody rigidBody;

    void Awake() {
        audioSource = GetComponent<AudioSource>();
        rigidBody = GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision coll) {
        audioSource.PlayOneShot(audioSource.clip);
    }

    public bool IsStanding() {
        bool evalX = System.Math.Abs(transform.rotation.eulerAngles.x) > standingThresholdRotation && System.Math.Abs(transform.rotation.eulerAngles.x) < 360 - standingThresholdRotation;
        bool evalZ = System.Math.Abs(transform.rotation.eulerAngles.z) > standingThresholdRotation && System.Math.Abs(transform.rotation.eulerAngles.x) < 360 - standingThresholdRotation;
        bool evalY = System.Math.Abs(transform.position.y) > standingThresholdPositionY;
        return !(evalX || evalZ || evalY);
    }

    public void setPinGravity(bool useGravity) {
        rigidBody.useGravity = useGravity;
    }
}
