﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class ActionMasterTest {

    ActionMaster.Action endTurn = ActionMaster.Action.EndTurn;
    ActionMaster.Action tidy = ActionMaster.Action.Tidy;
    ActionMaster.Action reset = ActionMaster.Action.Reset;
    ActionMaster actionMaster;

    [SetUp]
    public void Setup() {
        actionMaster = new ActionMaster();
    }

    [Test]
    public void T00_PassingTest() {
        Assert.AreEqual(1, 1);
    }

    [Test]
    public void T01_OneStrikeEndsTurn() {
        Assert.AreEqual(endTurn, actionMaster.Bowl(10));
    }

    [Test]
    public void T02_EightReturnsTidy() {
        Assert.AreEqual(tidy, actionMaster.Bowl(8));
    }

    [Test]
    public void T03_TwoBowlsEndTurn() {
        actionMaster.Bowl(8);
        Assert.AreEqual(endTurn, actionMaster.Bowl(2));
    }

    [Test]
    public void T04_StrikeIn19BowlReturnsReset() { 
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        Assert.AreEqual(reset, actionMaster.Bowl(10));
     //   Assert.AreEqual(reset, actionMaster.Bowl(10));
    }

    [Test]
    public void T05_StrikeIn20BowlReturnsReset() {
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        Assert.AreEqual(reset, actionMaster.Bowl(10));
    }

    [Test]
    public void T06_NonSpare20ReturnsEndTurn() {
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(7);
        Assert.AreEqual(endTurn, actionMaster.Bowl(2));
    }

    [Test]
    public void T07_Spare20ReturnsReset() {
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(7);
        Assert.AreEqual(reset, actionMaster.Bowl(3));
    }

    [Test]
    public void T08_Anything21ReturnsEndTurn() {
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        Assert.AreEqual(endTurn, actionMaster.Bowl(10));

        actionMaster = new ActionMaster();

        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        Assert.AreEqual(endTurn, actionMaster.Bowl(0));

        actionMaster = new ActionMaster();

        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        Assert.AreEqual(endTurn, actionMaster.Bowl(5));
    }

    [Test]
    public void T09_MiddleNonSpareReturnsEndTurn() {
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(7);
        Assert.AreEqual(endTurn, actionMaster.Bowl(2));
    }

    [Test]
    public void T10_MiddlePlayThrowReturnsTidy() {
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        Assert.AreEqual(tidy, actionMaster.Bowl(7));
    }

    [Test]
    public void T11_19StrikeMeans20ZeroReturnsTidy() {
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        Assert.AreEqual(tidy, actionMaster.Bowl(0));
    }

    [Test]
    public void T12_19StrikeMeans20OneReturnsTidy() {
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        Assert.AreEqual(tidy, actionMaster.Bowl(1));
    }

    [Test]
    public void T13_StrikeInSecondFrameDoesntDisruptNextTurn() {
        actionMaster.Bowl(0);
        actionMaster.Bowl(10);
        Assert.AreEqual(tidy, actionMaster.Bowl(1));
    }

    [Test]
    public void T14_StrikeInSecondFrameDoesntDisruptNextTurn2() {
        actionMaster.Bowl(0);
        actionMaster.Bowl(10);
        actionMaster.Bowl(5);
        Assert.AreEqual(endTurn, actionMaster.Bowl(1));
    }

    [Test]
    public void T15_PerfectEndingWorkingWell() {
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        actionMaster.Bowl(10);
        Assert.AreEqual(reset, actionMaster.Bowl(10));
        Assert.AreEqual(reset, actionMaster.Bowl(10));
        Assert.AreEqual(endTurn, actionMaster.Bowl(10));
    }

    // Bens tests

    [Test]
    public void ZZ_T05CheckResetAtStrikeInLastFrame() {
        int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
        foreach (int roll in rolls) {
            actionMaster.Bowl(roll);
        }
        Assert.AreEqual(reset, actionMaster.Bowl(10));
    }

    [Test]
    public void ZZ_T06CheckResetAtSpareInLastFrame() {
        int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
        foreach (int roll in rolls) {
            actionMaster.Bowl(roll);
        }
        actionMaster.Bowl(1);
        Assert.AreEqual(reset, actionMaster.Bowl(9));
    }

    [Test]
    public void ZZ_T07YouTubeRollsEndInEndGame() {
        int[] rolls = { 8, 2, 7, 3, 3, 4, 10, 2, 8, 10, 10, 8, 0, 10, 8, 2 };
        foreach (int roll in rolls) {
            actionMaster.Bowl(roll);
        }
        Assert.AreEqual(endTurn, actionMaster.Bowl(9));
    }

    [Test]
    public void ZZ_T08GameEndsAtBowl20() {
        int[] rolls = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
        foreach (int roll in rolls) {
            actionMaster.Bowl(roll);
        }
        Assert.AreEqual(endTurn, actionMaster.Bowl(1));
    }








    /* memento
    [Test]
    public void ActionMasterTestSimplePasses() {
        // Use the Assert class to test conditions.
    }

    // A UnityTest behaves like a coroutine in PlayMode
    // and allows you to yield null to skip a frame in EditMode
    [UnityTest]
    public IEnumerator ActionMasterTestWithEnumeratorPasses() {
        // Use the Assert class to test conditions.
        // yield to skip a frame
        yield return null;
    }
    */
}
