﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    // tutorial changes velocity directly ATM. Maybe refactor after tutorial? If so, Launch() core is rigidBody.AddForce(0, 0, initialForce); ~45000  initial force worked at the moment of redesign
    // [SerializeField]
    //  float initialForce = 5000f; 

    [SerializeField]
    Vector3 launchVelocity;
    [SerializeField]
    float maxStartXDeviation;

    //cache
    Rigidbody rigidBody;
    AudioSource audioSource;
    PinSetter pinsSetter; 

    //state
   // float startingXPosition;
    Vector3 startingPosition;
    bool isLaunched = false;


    void Awake() {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        pinsSetter = FindObjectOfType<PinSetter>();
    }

    // Use this for initialization
    void Start() {
        rigidBody.useGravity = false;
        startingPosition = transform.position;
        //     Launch(launchVelocity);
    }

    public void Launch(Vector3 launchVelocity) {
        if (!isLaunched) {
            isLaunched = true;
            rigidBody.useGravity = true;
            rigidBody.velocity = launchVelocity;
            audioSource.Play();
        }
    }

    public void MoveStart(float distance) {
        if (!isLaunched) {
            float newXPos = Mathf.Clamp(rigidBody.position.x + distance, startingPosition.x - maxStartXDeviation, startingPosition.x + maxStartXDeviation);
            rigidBody.position = new Vector3(newXPos, rigidBody.position.y, rigidBody.position.z);
        }
    }

    public void Reset() {
        rigidBody.velocity = new Vector3(0f, 0f, 0f);
        rigidBody.angularVelocity = new Vector3(0f, 0f, 0f);
        transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
        transform.position = startingPosition;
        isLaunched = false;
    }

    void OnTriggerExit(Collider coll) {
        GutterBall gb = coll.GetComponent<GutterBall>();
        if(gb) {
            pinsSetter.SetBallLeft();
        }
    }
}
