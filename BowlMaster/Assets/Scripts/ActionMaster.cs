﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionMaster {

    int[] bowls = new int[21];
    int bowl = 1;

    public enum Action {
        Tidy, Reset, EndTurn, EndGame
    }

	// Use this for initialization
	void Start () {
    }

    // Update is called once per frame
    void Update () {
		
	}

    /* Method returns action to perfrom after playing one bowl
     * 
     * Premise of method is, that we have different actions taken in last frame (bowls 19-21) and in normal frames (strikes/non strikes).
     * While working with arrays, array is accesed by substraction to better visualize number of bowl played - the substraction removes the offset caused by array starting at [0].
     * 
     * */
    public Action Bowl(int pins) {
        if(pins > 10 || pins < 0) {
            throw new UnityException("Invalid pin count passed to ActionMaster.Bowl");
        }
        bowls[bowl - 1] = pins;
        /* last turn STARTS*/
        if(pins == 10 && bowl == 19) {
            bowl += 1;
            return Action.Reset;
        }
        if(bowl == 20) {
            bowl += 1;
            if(pins != 10 && bowls[19-1] == 10) {
                return Action.Tidy;
            }
            if(bowls[19-1] + bowls[20-1] == 10 || bowls[20-1] == 10) {
                return Action.Reset;
            }
            bowls[21-1] = 0;
            return Action.EndTurn;
        }
        if (bowl == 21) {
            return Action.EndTurn;
        }
        /* last turn ENDS */
        /* Strikes */
        if (pins == 10) {
            if (bowl % 2 == 1) {
                bowl += 2;
                bowls[bowl - 2] = -1; // One to get last round, another one to offset for array start at 0
            } else {
                bowl += 1;
            }
            return Action.EndTurn;
        }
        /* Normal turns */
        if(bowl % 2 == 1) {
            bowl += 1;
            return Action.Tidy;
        }
        if(bowl % 2 == 0) {
            bowl += 1;
            return Action.EndTurn;
        }

        throw new UnityException("Action bowl does not not know, hat action should be taken next");
    }

    public int GetSumOfBowls() {
        int sum = 0;
        foreach(int num in bowls) {
            if(num != -1) {
                sum += num;
            }
        }
        return sum;
    }
}
