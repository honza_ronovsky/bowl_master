﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Ball))]
public class DragLaunch : MonoBehaviour {

    [SerializeField]
    Vector3 dragModifier;

    [SerializeField]
    float maxSpeed;

    //cache
    Ball ball;
    Vector3 dragStartPosition;
    float dragStartTime;

	// Use this for initialization
	void Start () {
        ball = GetComponent<Ball>();
	}

    public void DragStart() {
        dragStartPosition = Input.mousePosition;
        dragStartTime = Time.time;
    }

    public void DragEnd() {
        Vector3 dragEndPosition = Input.mousePosition;
        float dragEndTime = Time.time;
        float timeDiff = dragEndTime - dragStartTime;
        float zPoz = Mathf.Clamp((dragStartPosition.y - dragEndPosition.y) / timeDiff, -maxSpeed, maxSpeed); //* dragModifier.z;
        float xPoz = Mathf.Clamp((dragStartPosition.x - dragEndPosition.x) / timeDiff, -maxSpeed, maxSpeed); //* dragModifier.z;
        Debug.Log("Z: " + zPoz + " X: " + xPoz);
        Vector3 launchVector; //= (dragEndPosition - dragStartPosition) * dragModifier.z;
        launchVector = new Vector3(xPoz/100, 0, zPoz/100);
        ball.Launch(launchVector);
    }
}
