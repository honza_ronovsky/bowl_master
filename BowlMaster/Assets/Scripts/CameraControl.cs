﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {
    //design
    [SerializeField]
    GameObject focusGameObject;
    [SerializeField]
    Vector3 distanceMaintained;
    [SerializeField]
    GameObject stopObject;
    [SerializeField]
    Vector3 stopObjectDistance;

    //state
   // Vector3 startingPosition;

	// Use this for initialization
	void Start () {
        //startingPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if(stopObject.transform.position.z - focusGameObject.transform.position.z > stopObjectDistance.z) {
            transform.position = focusGameObject.transform.position + distanceMaintained;
        }
	}

    public void SetStopObject(GameObject stopObject) {
        this.stopObject = stopObject;
    }

    /*
    public void Reset() {
        transform.position = startingPosition;
    }*/
}
